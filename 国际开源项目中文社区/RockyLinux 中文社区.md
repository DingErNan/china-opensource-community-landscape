# RockyLinux 中文社区

![avatar](https://www.rockylinux.cn/wp-content/uploads/2022/11/Rocky_Linux_logo.png)

## 社区简介

Rocky Linux 中文社区，成立于2020年12月10日，社区运营着微信公众号、QQ群和微信群，拥有数千名 Linux 爱好者（微信公从号：：`RockyLinux`）。自成立以来，Rocky Linux 中文社区发表了大量的博客文章，并对社区官方进行了升级改版，开通了对应的BBS 社区论坛，为 Linux 爱好者提供了高效的交流平台。

Rocky Linux 中文社区宗旨：致力于 Rocky Linux 在中文地区的发展。

Rocky Linux 中文社区，是一个非盈利性质的开源技术社区，社区官网：www.rockylinux.cn，目前每天平均访客量达几百人，有来自 43 个国家和地区的访客。

社区成立之初，完成了对 [Rocky Linux 官方网站](https://rockylinux.org/)的【简体中文】语言翻译工作，以满足中文地区最早一批 Rocky Linux 爱好者的需求，积极有效的推动了 Rocky Linux 在中文社区的落地。

## 社区治理模式

为更好地服务于中文地区 Rocky Linux 爱好者，社区在主要核⼼成员中设立：主席、副主席、核心成员组。并设立事务小组，包括：文档翻译组、资讯维护组、社区管理组、国外联络组、国内外联组等，还组建了不同的区域分会，包括：北京站、深圳站、上海站、成都站等，以满足未来社区活动需求。